<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/functions.php';

print "Q1: Given ‘from’ and ‘to’, please order the routes by the lowest number of exchanges.\n\n";

$routesGenerator = new \Generators\RoutesGenerator();
$routes = $routesGenerator->generate();

$resolver = new \Travel\DfsRouteResolver($routes);
$findings = $resolver->from('A')->to('C')->orderByLessExchanges()->find();

$printer = new \Travel\RoutePrinter($findings);
$printer->print();