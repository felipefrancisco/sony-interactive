<?php

/**
 * Class RouteClass
 */
class RouteClass
{
	private $from;
	private $to;
	private $duration;

    /**
     * RouteClass constructor.
     * @param $from
     * @param $to
     * @param $duration
     */
    public function __construct($from, $to, $duration) {
		$this->from = $from;
		$this->to = $to;
		$this->duration = $duration;
	}

    /**
     * @return array
     */
    public function getRoute() {
		return [
			'route_id' => $this->from . '_' . $this->to,
			'from' => $this->from,
			'to' => $this->to,
			'duration' => $this->duration
		];
	}

}

/**
 * Class FlightClass
 */
class FlightClass
{
	private $flight_id;
	private $route;
	private $provider;
	private $price;
	private $departure;

    /**
     * FlightClass constructor.
     * @param $flight_id
     * @param $route
     * @param $provider
     * @param $price
     * @param $departure
     */
    public function __construct($flight_id, $route, $provider, $price, $departure) {
		$this->flight_id = $flight_id;
		$this->route = $route;
		$this->provider = $provider;
		$this->price = $price;
		$this->departure = $departure;
	}

    /**
     * @return array
     */
    public function getFlight() {
		return [
			'flight_id' => $this->flight_id,
			'route' => $this->route,
			'provider' => $this->provider,
			'price' => $this->price,
			'departure' => $this->departure
		];
	}
}


// generate the routes
$routes = array();
$locations = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
foreach ($locations as $from) {

    $excludeList = array();
    // $from should be excluded from destination
    $excludeList[] = $from;

    // skip some destinations too
    $excludeList[] = $locations[rand(0, count($locations)-1)];
    $excludeList[] = $locations[rand(0, count($locations)-1)];
    $excludeList[] = $locations[rand(0, count($locations)-1)];

    $destinations = array_diff($locations, $excludeList);

    foreach($destinations as $key => $to) {
        $hour = rand(1, 12);
        $route = new RouteClass($from, $to, $hour);
        $routes[] = $route->getRoute();
    }
}

var_dump(json_encode($routes));

// generate the flights
$flights = array();
$providers = ['Virgin', 'BA', 'Ryanair', 'EasyJet'];
$int = 1;
foreach($routes as $key => $route) {
    // assume that one provider does not provide flight for this route
    $unavailableProviders = [$providers[rand(0, 3)]];
    $availableProvides = array_diff($providers, $unavailableProviders);
    foreach($availableProvides as $provider) {
        $price = rand(100, 1200);
        $departureDateTime = sprintf('+%s day %s hour', rand(1, 10), rand(1, 12));
        $flight = new FlightClass($int, $route['route_id'], $provider, $price, $departureDateTime);
        $int++;
        $flights[] = $flight->getFlight();
    }
}

var_dump(json_encode($flights));