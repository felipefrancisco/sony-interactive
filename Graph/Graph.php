<?php

namespace Graph;

/**
 * Class Graph
 *
 * @package Graph
 */
class Graph
{
    /**
     * @var array
     */
    public $linkedList = [];

    /**
     * @param $source
     * @param $destination
     * @param $duration
     * @param $id
     */
    public function addEdge($source, $destination, $duration, $id)
    {
        $this->linkedList[$source][] = new Node($source, $destination, $duration, $id);
    }
}