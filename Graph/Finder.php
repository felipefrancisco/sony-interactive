<?php

namespace Graph;

/**
 * Class Finder
 * @package Graph
 */
class Finder
{
    /**
     * @var array
     */
    protected static $findings = [];

    /**
     * @param Graph $graph
     * @param $start
     * @param $end
     * @param $path
     * @param $visited
     * @param int $duration
     * @param array $routesUsed
     * @param null $previousNode
     * @return array
     */
    public function find(Graph $graph, $start, $end, $path, $visited, $duration = 0, $routesUsed = [], $previousNode = null)
    {
        $fullPath = sprintf("%s->%s", $path, $start);
        if ($previousNode) {
           $routesUsed[] = $previousNode->id;
        }
        $visited[$start] = true;

        if (!isset($graph->linkedList[$start])) {
            $graph->linkedList[$start] = [];
        }

        $list = $graph->linkedList[$start];

        // Iterate through the linked list
        for ($i = 0; $i < count($list) ; $i++) {

            $node = $list[$i];

            // If the destination was never visited before, create the default "visited = false"
            // entry to avoid notices.
            if (!isset($visited[$node->destination])) {
                $visited[$node->destination] = false;
            }

            // If the destination of this node is not the final destination (and it hasn't been
            // visited before), perform the search into this node.
            if ($node->destination != $end && $visited[$node->destination] == false){
                $totalDuration = ($duration + $node->duration);
                $visited[$node->destination] = true;
                $this->find($graph,$node->destination,$end,$fullPath,$visited, $totalDuration, $routesUsed, $node);
            }
            elseif($node->destination == $end){
                $totalDuration = ($duration + $node->duration);

                if (empty($routesUsed)) {
                    $routesUsed[] = $node->id;
                }

                $path = sprintf("%s->%s", $fullPath, $node->destination);

                self::$findings[] = [
                    'path' => $path,
                    'totalDuration' => $totalDuration,
                    'routesUsed' => $routesUsed
                ];
            }
        }

        // Remove this from the path
        $visited[$start] = false;

        return self::$findings;
    }

    /**
     * @param $graph
     * @param $start
     * @param $end
     * @return array
     */
    public function findAll($graph,  $start, $end){

        $visited = [];
        $visited[$start] = true;
        return $this->find($graph, $start, $end, "", $visited);
    }
}