<?php

namespace Graph;

/**
 * Class Node
 *
 * @package Graph
 */
class Node
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $source;

    /**
     * @var string
     */
    public $destination;

    /**
     * @var string
     */
    public $duration;

    /**
     * Node constructor.
     *
     * @param $source
     * @param $destination
     * @param $duration
     * @param $id
     */
    public function __construct($source, $destination, $duration, $id)
    {
        $this->id = $id;
        $this->source = $source;
        $this->destination = $destination;
        $this->duration = $duration;
    }
}