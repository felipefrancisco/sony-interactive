<?php

namespace Travel\Entities;

/**
 * Class Flight
 *
 * @package Travel
 */
class Flight
{

    /**
     * @var stirng
     */
    private $flightId;

    /**
     * @var stirng
     */
    private $route;

    /**
     * @var stirng
     */
    private $provider;

    /**
     * @var stirng
     */
    private $price;

    /**
     * @var stirng
     */
    private $departure;

    /**
     * Flight constructor.
     *
     * @param $fightId
     * @param $route
     * @param $provider
     * @param $price
     * @param $departure
     */
    public function __construct(string $fightId, string $route, string $provider, float $price, string $departure)
    {
        $this->flightId = $fightId;
        $this->route = $route;
        $this->provider = $provider;
        $this->price = $price;
        $this->departure = $departure;
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        return [
            'flightId' => $this->flightId,
            'route' => $this->route,
            'provider' => $this->provider,
            'price' => $this->price,
            'departure' => $this->departure
        ];
    }
}