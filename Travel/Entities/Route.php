<?php

namespace Travel\Entities;

/**
 * Class Flight
 *
 * @package Route
 */
class Route
{

    /**
     * @var string
     */
    private $from;

    /**
     * @var string
     */
    private $to;

    /**
     * @var int
     */
    private $duration;

    /**
     * Route constructor.
     *
     * @param $from
     * @param $to
     * @param $duration
     */
    public function __construct(string $from, string $to, int $duration)
    {
        $this->from = $from;
        $this->to = $to;
        $this->duration = $duration;
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        return [
            'route_id' => $this->routeId(),
            'from' => $this->from,
            'to' => $this->to,
            'duration' => $this->duration
        ];
    }

    /**
     * @return string
     */
    public function routeId() : string
    {
        return $this->from . '_' . $this->to;
    }
}