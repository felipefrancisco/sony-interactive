<?php

namespace Travel;

use Graph\Graph;
use Graph\Printer;

/**
 * Class FlightResolver
 * @package Travel
 */
class FlightResolver
{
    /**
     * @var array
     */
    protected $flights;

    /**
     * @var array
     */
    protected $foundRoutes;

    /**
     * @var int
     */
    protected $departure;

    /**
     * @var bool
     */
    protected $orderByPrice = false;

    /**
     * FlightResolver constructor.
     * @param array $flights
     * @param array $foundRoutes
     * @param int $departure
     */
    public function __construct(array $flights, array $foundRoutes, int $departure)
    {
        $this->flights = $flights;
        $this->foundRoutes = $foundRoutes;
        $this->departure = $departure;

        $this->reorganizeFlights();
    }

    /**
     * @return $this
     */
    public function orderByPrice()
    {
        $this->orderByPrice = true;
        return $this;
    }

    protected function reorganizeFlights()
    {
        $flightsByRoute = [];
        foreach ($this->flights as $flight) {
            $flight['departure'] = strtotime($flight['departure']);

            $id = $flight['route'];

            if (!isset($flightsByRoute[$id])) {
                $flightsByRoute[$id] = [];
            }

            if ($flight['departure'] < $this->departure) {
                continue;
            }

            $flightsByRoute[$id][] = $flight;
        }

        foreach ($flightsByRoute as $routeId => $flights) {

            usort($flights, 'sortByPrice');
            $flightsByRoute[$routeId] = $flights;
        }

        $this->flights = $flightsByRoute;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function find()
    {
        foreach ($this->foundRoutes as &$route) {

            $routesUsed = $route['routesUsed'];
            $route['price'] = 0;

            foreach ($routesUsed as $routeId) {

                if (!isset($this->flights[$routeId])) {
                    //throw new \Exception(sprintf('No flights for route: %s', $routeId));
                    continue;
                }

                $price = current($this->flights[$routeId])['price'];
                $route['price'] += $price;
            }
        }

        if ($this->orderByPrice) {
            usort($this->foundRoutes, 'sortByPrice');
        }

        return $this->foundRoutes;
    }
}