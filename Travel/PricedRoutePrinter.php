<?php

namespace Travel;

/**
 * Class PricedRoutePrinter
 * @package Travel
 */
class PricedRoutePrinter
{
    /**
     * @var array
     */
    protected $findings;

    /**
     * @var string
     */
    protected $message = 'Route %s, price is %s';

    /**
     * PricedRoutePrinter constructor.
     * @param array $findings
     */
    public function __construct(array $findings)
    {
        $this->findings = $findings;
    }

    /**
     * @return string
     */
    protected function generator()
    {
        foreach ($this->findings as $route => $data) {

            $message = sprintf(
                $this->message,
                $data['path'],
                $data['price']
            );

            echo $message . "\n";
        }

        return;
    }

    public function print()
    {
        echo $this->generator();
    }
}