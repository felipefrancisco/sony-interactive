<?php

namespace Travel;

use Graph\Graph;
use Graph\Finder;

/**
 * Class DfsRouteResolver
 * @package Travel
 */
class DfsRouteResolver
{
    /**
     * @var array
     */
    protected $routes;

    /**
     * @var string
     */
    protected $from;

    /**
     * @var string
     */
    protected $to;

    /**
     * @var bool
     */
    protected $orderByDuration = false;

    /**
     * @var bool
     */
    protected $orderByLessExchanges = false;

    /**
     * DfsRouteResolver constructor.
     * @param array $routes
     */
    public function __construct(array $routes)
    {
        $this->routes = $routes;
    }

    /**
     * @param string $from
     * @return $this
     */
    public function from(string $from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * @param string $to
     * @return $this
     */
    public function to(string $to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * @return $this
     */
    public function orderByDuration()
    {
        $this->orderByDuration = true;

        return $this;
    }

    /**
     * @return $this
     */
    public function orderByLessExchanges()
    {
        $this->orderByLessExchanges = true;

        return $this;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function find()
    {
        $graph = new Graph();

        foreach ($this->routes as $route) {

            $from = $route['from'];
            $to = $route['to'];
            $duration = $route['duration'];
            $id = $route['route_id'];

            $graph->addEdge($from, $to, $duration, $id);
        }

        $p = new Finder();
        $paths = $p->findAll($graph, $this->from, $this->to);

        if (!count($paths)) {
            throw new \Exception('No paths found');
        }

        if ($this->orderByDuration) {
            usort($paths, 'sortByDuration');
        }

        if ($this->orderByLessExchanges) {
            usort($paths, 'sortByLessExchanges');
        }

        return $paths;
    }
}