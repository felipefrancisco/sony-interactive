<?php

namespace Travel;

/**
 * Class RoutePrinter
 * @package Travel
 */
class RoutePrinter
{
    /**
     * @var array
     */
    protected $findings;

    /**
     * @var string
     */
    protected $message = 'Route %s, duration is %s';

    /**
     * RoutePrinter constructor.
     * @param array $findings
     */
    public function __construct(array $findings)
    {
        $this->findings = $findings;
    }

    /**
     * @return string
     */
    protected function generator()
    {
        foreach ($this->findings as $route => $data) {

            $message = sprintf(
                $this->message,
                $data['path'],
                $data['totalDuration']
                // implode(',', $data['routesUsed'])
            );

            echo $message . "\n";
        }

        return;
    }

    public function print()
    {
        echo $this->generator();
    }
}