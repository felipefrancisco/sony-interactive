<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/functions.php';

print "Q3: Given ‘from’ and ‘to’, and the departure time, please order the results by the shortest total duration time.\n\n";

$routesGenerator = new \Generators\RoutesGenerator();
$routes = $routesGenerator->generate();

$resolver = new \Travel\DfsRouteResolver($routes);
$findings = $resolver->from('A')->to('C')->orderByDuration()->find();

$flightsGenerator = new \Generators\FlightsGenerator($routes);
$flights = $flightsGenerator->generate();

$departure = time();

$resolver = new \Travel\FlightResolver($flights, $findings, $departure);
$findings = $resolver->find();

$printer = new \Travel\RoutePrinter($findings);
$printer->print();