<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/functions.php';

print "Q2: Given ‘from’ and ‘to’, and the departure time, please order the results by the cheapest total price.\n\n";

$routesGenerator = new \Generators\RoutesGenerator();
$routes = $routesGenerator->generate();

$resolver = new \Travel\DfsRouteResolver($routes);
$findings = $resolver->from('A')->to('C')->find();

$flightsGenerator = new \Generators\FlightsGenerator($routes);
$flights = $flightsGenerator->generate();

$departure = time();

$resolver = new \Travel\FlightResolver($flights, $findings, $departure);
$findings = $resolver->orderByPrice()->find();

$printer = new \Travel\PricedRoutePrinter($findings);
$printer->print();