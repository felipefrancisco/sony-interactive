## Sony Interactive Test

### Configuration

Install Dependencies
```bash
composer install
```

### Files Disposition

There are 3 files corresponding to the tasks in the test: `q1.php`, `q2.php` and `q3.php`. They can be run by using:
```bash
php q1.php
php q2.php
php q3.php
```