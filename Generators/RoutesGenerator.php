<?php

namespace Generators;

use Generators\Contract\GeneratorContract;
use Travel\Entities\Route;

/**
 * Class RoutesGenerator
 * @package Generators
 */
class RoutesGenerator implements GeneratorContract
{
    /**
     * @return array
     */
    public function generate() : array
    {
        $routes = [];
        $locations = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
        foreach ($locations as $from) {

            $excludeList = [];

            // $from should be excluded from destination
            $excludeList[] = $from;

            // skip some destinations too
            $excludeList[] = $locations[rand(0, count($locations)-1)];
            $excludeList[] = $locations[rand(0, count($locations)-1)];
            $excludeList[] = $locations[rand(0, count($locations)-1)];

            $destinations = array_diff($locations, $excludeList);

            foreach($destinations as $key => $to) {
                $hour = rand(1, 12);
                $route = new Route($from, $to, $hour);
                $routes[] = $route->toArray();
            }
        }

        return $routes;
    }
}