<?php

namespace Generators\Contract;


/**
 * Interface GeneratorContract
 * @package Generators\Contract
 */
interface GeneratorContract
{
    /**
     * @return array
     */
    public function generate() : array;
}