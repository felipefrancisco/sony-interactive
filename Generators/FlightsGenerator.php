<?php

namespace Generators;

use Generators\Contract\GeneratorContract;
use Travel\Entities\Flight;

/**
 * Class FlightsGenerator
 * @package Generators
 */
class FlightsGenerator implements GeneratorContract
{
    protected $routes;

    /**
     * FlightsGenerator constructor.
     * @param array $routes
     */
    public function __construct(array $routes)
    {
        $this->routes = $routes;
    }

    /**
     * @return array
     */
    public function generate() : array
    {
        $flights = [];
        $providers = ['Virgin', 'BA', 'Ryanair', 'EasyJet'];
        $int = 1;

        foreach ($this->routes as $key => $route) {
            // assume that one provider does not provide flight for this route
            $unavailableProviders = [$providers[rand(0, 3)]];
            $availableProvides = array_diff($providers, $unavailableProviders);
            foreach ($availableProvides as $provider) {
                $price = rand(100, 1200);
                $departureDateTime = sprintf('+%s day %s hour', rand(1, 10), rand(1, 12));
                $flight = new Flight($int, $route['route_id'], $provider, $price, $departureDateTime);
                $int++;
                $flights[] = $flight->toArray();
            }
        }

        return $flights;
    }
}