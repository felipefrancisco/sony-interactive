<?php

/**
 * @param $a
 * @param $b
 * @return int
 */
function sortByLessExchanges($a, $b) {
    return strlen($a['path']) - strlen($b['path']);
}

/**
 * @param $a
 * @param $b
 * @return mixed
 */
function sortByDuration($a, $b) {
    return $a['totalDuration'] - $b['totalDuration'];
}

/**
 * @param $a
 * @param $b
 * @return mixed
 */
function sortByPrice($a, $b) {
    return $a['price'] - $b['price'];
}